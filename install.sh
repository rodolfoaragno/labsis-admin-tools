#!/bin/sh

echo "instalando software..."
install -o root -m 755 maquinas-prendidas /usr/local/bin

echo "instalando manuales..."
install -o root -m 644 maquinas-prendidas.1 /usr/local/man/man1

echo "listo"
